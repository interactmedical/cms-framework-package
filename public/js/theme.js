$(function () {

  // navbar notification popups
  $(".notification-dropdown").each(function (index, el) {
    var $el = $(el);
    var $dialog = $el.find(".pop-dialog");
    var $trigger = $el.find(".trigger");
    
    $dialog.click(function (e) {
        e.stopPropagation()
    });
    $dialog.find(".close-icon").click(function (e) {
      e.preventDefault();
      $dialog.removeClass("is-visible");
      $trigger.removeClass("active");
    });
    $("body").click(function () {
      $dialog.removeClass("is-visible");
      $trigger.removeClass("active");
    });

    $trigger.click(function (e) {
      e.preventDefault();
      e.stopPropagation();
      
      // hide all other pop-dialogs
      $(".notification-dropdown .pop-dialog").removeClass("is-visible");
      $(".notification-dropdown .trigger").removeClass("active")

      $dialog.toggleClass("is-visible");
      if ($dialog.hasClass("is-visible")) {
        $(this).addClass("active");
      } else {
        $(this).removeClass("active");
      }
    });
  });


  // skin changer
  $(".skins-nav .skin").click(function (e) {
    e.preventDefault();
    if ($(this).hasClass("selected")) {
      return;
    }
    $(".skins-nav .skin").removeClass("selected");
    $(this).addClass("selected");
    
    if (!$("#skin-file").length) {
      $("head").append('<link rel="stylesheet" type="text/css" id="skin-file" href="">');
    }
    var $skin = $("#skin-file");
    if ($(this).attr("data-file")) {
      $skin.attr("href", $(this).data("file"));
    } else {
      $skin.attr("href", "");
    }

  });


  // sidebar menu dropdown toggle
  $("#dashboard-menu .dropdown-toggle").click(function (e) {
    e.preventDefault();
    var $item = $(this).parent();
    $item.toggleClass("active");
    if ($item.hasClass("active")) {
      $item.find(".submenu").slideDown("fast");
    } else {
      $item.find(".submenu").slideUp("fast");
    }
  });


  // mobile side-menu slide toggler
  var $menu = $("#sidebar-nav");
  $("body").click(function () {
    if ($(this).hasClass("menu")) {
      $(this).removeClass("menu");
    }
  });
  $menu.click(function(e) {
    e.stopPropagation();
  });
  $("#menu-toggler").click(function (e) {
    e.stopPropagation();
    $("body").toggleClass("menu");
  });
  $(window).resize(function() { 
    $(this).width() > 769 && $("body.menu").removeClass("menu")
  })


	// build all tooltips from data-attributes
	$("[data-toggle='tooltip']").each(function (index, el) {
		$(el).tooltip({
			placement: $(this).data("placement") || 'top'
		});
	});


  // custom uiDropdown element, example can be seen in user-list.html on the 'Filter users' button
	var uiDropdown = new function() {
  	var self;
  	self = this;
  	this.hideDialog = function($el) {
    		return $el.find(".dialog").hide().removeClass("is-visible");
  	};
  	this.showDialog = function($el) {
    		return $el.find(".dialog").show().addClass("is-visible");
  	};
		return this.initialize = function() {
  		$("html").click(function() {
    		$(".ui-dropdown .head").removeClass("active");
      		return self.hideDialog($(".ui-dropdown"));
    		});
    		$(".ui-dropdown .body").click(function(e) {
      		return e.stopPropagation();
    		});
    		return $(".ui-dropdown").each(function(index, el) {
      		return $(el).click(function(e) {
      			e.stopPropagation();
      			$(el).find(".head").toggleClass("active");
      			if ($(el).find(".head").hasClass("active")) {
        			return self.showDialog($(el));
      			} else {
        			return self.hideDialog($(el));
      			}
      		});
    		});
    	};
  	};

    // instantiate new uiDropdown from above to build the plugins
  	new uiDropdown();
  	
  	// toggle all checkboxes from a table when header checkbox is clicked
  	$(".table th input:checkbox").click(function () {
  		$checks = $(this).closest(".table").find("tbody input:checkbox");
  		if ($(this).is(":checked")) {
  			$checks.prop("checked", true);
  		} else {
  			$checks.prop("checked", false);
  		}  		
  	});

	$('.delete').on('click', function(e){
		e.preventDefault();
		
		var url = $(this).attr('href');
		$.ajax({
			url: url,
			method: 'get'
		}).done(function(data){
			if (data != 0) {
				alert('You must empty this item before deleting.');
			} else {
				if (confirm("Are you sure you want to delete this item?")) {
					$.ajax({
						url: url,
						method: 'delete'
					}).done(function(){
						location.reload();
					});
				}
			}
		});
		
	});

	$('.filter').change(function(){
		
		$(this).val();
		
		$.ajax({
			url: '../filters',
			method: 'post',
			data: { filter: $(this).find(':selected').text() }
		}).done(function(result){
			$.each($.parseJSON(result), function(key,value){
				var container = $('.'+key);
				container.removeClass('hidden');
				container.toggle(!value);
				if (container.find(':checkbox').length > 0) {
					if (container.css('display') != 'none') {
						container.find(':checkbox').prop('checked', true);
						container.find(':checkbox').attr('checked', true);	
					} else {
						container.find(':checkbox').prop('checked', false);
						container.find(':checkbox').attr('checked', false);	
					}
				} 
			});
		});
		
	});

	$('.init-select').prop('selectedIndex', -1);
	
	$('.sortable').sortable({
		start: function(){
			$(this).find('.sort-hide').hide();
			$(this).css('cursor', 'pointer');
		},
		stop: function() {
			$(this).find('.sort-hide').show();
			$(this).css('cursor', 'default');
		},
		update: function() {
			var order = $('.sortable').sortable('serialize');
			$.ajax({
				url: window.location.pathname+'/sort',
				method: 'post',
				data: order
			});
		}
	}).disableSelection();
		
	$('.fileupload').fileupload({
		dataType: 'html',
		autoUpload: false,
		done: function(e, data) {
			if(data.result.indexOf("http") >= 0) {
				window.location = data.result;	
			} else {
				$('#progress .progress-bar').css(
					'width', 0+'%'
				).text(0+'%');
				alert(data.result);
			}
		},
		add: function(e, data){
			var form = $(this).parent('div').parent('div').parent('div').parent('form');
			var submit = form.find(':submit');
			$(this).parent('span').parent('div').siblings('.filename').text(data.files[0].name);
			//console.log(data.files[0].name);
			data.context = $(':submit').click(function(e){
				e.preventDefault();
				$('#ajax').val(true);
				data.submit();
			});
		},
		progressall: function(e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#progress .progress-bar').css(
				'width', progress+'%'
			).text(progress+'%');
		}
	});

});