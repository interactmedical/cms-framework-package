# Interact CMS Framework

This framework allows for the rapid development of CMS (Content Management Systems) originially intended for Interact Medical development

## Installation

Begin by installing this package using composer. Edit the base `composer.json` file to require interact/cms.

    "require" : {
    	"laravel/framework" : "4.1.*",
    	"interact/cms" : "dev-master"
    }

Update Composer from the terminal
    composer update
    
Add the service provider to the project in `app/config/app.php`

    'Interact\Cms\CmsServiceProvider'
    
### Create the database

Create a database and edit the `app/config/database.php` file with the credentials

### Initialize a new project

Create an update and resources table in the database, and initialize JSON Controller

    php artisan initialize_cms_project

### Resource Models and Controllers

Use artisan to create resource database migrations and model controllers, views and models. Models should extend the CMSModel class, which is an extension of the Eloquent Model class. Examples as follows:

#### Create Migration, Table, Controller, Views, and Model
	php artisan create_cms_resource {resourcename} 

### Publish Assets

These are the images, css and javascript files for the project. To copy them from the CMS install package to the project use the following command:

    php artisan asset:publish interact/cms

### Copy configuration file

This holds the structure for the JSON file that communicates with the application. To create a project specific configuration, run the following command:

    php artisan config:publish interact/cms

## License

Copyright 2014 Interact Medical. No derivative works may be created. This package may not be distributed.
