@extends('cms::template.main')

@section('styles')
	@parent
	<!-- this page specific styles -->
@endsection

<!-- will be used to show any messages -->
@if (Session::has('error'))
	<div class="alert-error alert">
		<a class="close" data-dismiss="alert" href="#">&times;</a>
		<strong>{{ Session::get('error') }}</strong>
	</div>
@endif

@section('content')

	{{ HTML::image('uploads/logo/logo.png', '', array('style' => 'display: block; margin: 0 auto; padding: 100px 0')) }}

@stop