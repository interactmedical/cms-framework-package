<!DOCTYPE html>
<html>
<head>	
	<title>{{-- CMS()::title --}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/bootstrap/bootstrap-overrides.css') }}">

    <!-- libraries -->
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/lib/jquery-ui-1.10.2.custom.css') }}">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/lib/bootstrap-wysihtml5.css') }}">

    <!-- global styles -->
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/compiled/layout.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/compiled/elements.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/compiled/icons.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/screen.css') }}">

    <!-- this page specific styles -->
    @section('styles')
    <link rel="stylesheet" href="{{ asset('packages/interact/cms/css/compiled/index.css') }}" type="text/css" media="screen" />
	@show
	
    <!-- open sans font -->
    {{ HTML::style('//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800') }}

    <!-- lato font -->
    {{ HTML::style('//fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic') }}

    <!--[if lt IE 9]>
   	  {{ HTML::script('http://html5shim.googlecode.com/svn/trunk/html5.js') }}
    <![endif]-->
</head>
<body>
	
	@section('navbar')
	<header class="navbar navbar-inverse" role="banner">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" id="menu-toggler">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
                <span class="fa fa-bars"></span>
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">{{ Config::get('cms::cms_data')['companyname'] }} CMS</a>
        </div>
        @section('menu-drawer')
        <ul class="nav navbar-nav pull-right hidden-xs">
            <li class="hidden-xs hidden-sm">
                <!--
               	<input class="search" type="text" />
                -->
            </li>
            <li class="notification-dropdown hidden-xs hidden-sm">
            	<!--
                <a href="#" class="trigger">
                    <i class="icon-warning-sign"></i>
                    <span class="count">8</span>
                </a>
                <div class="pop-dialog">
                    <div class="pointer right">
                        <div class="arrow"></div>
                        <div class="arrow_border"></div>
                    </div>
                    <div class="body">
                        <a href="#" class="close-icon"><i class="icon-remove-sign"></i></a>
                        <div class="notifications">
                            <h3>You have 6 new notifications</h3>
                            <a href="#" class="item">
                                <i class="icon-signin"></i> New user registration
                                <span class="time"><i class="icon-time"></i> 13 min.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-signin"></i> New user registration
                                <span class="time"><i class="icon-time"></i> 18 min.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-envelope-alt"></i> New message from Alejandra
                                <span class="time"><i class="icon-time"></i> 28 min.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-signin"></i> New user registration
                                <span class="time"><i class="icon-time"></i> 49 min.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-download-alt"></i> New order placed
                                <span class="time"><i class="icon-time"></i> 1 day.</span>
                            </a>
                            <div class="footer">
                                <a href="#" class="logout">View all notifications</a>
                            </div>
                        </div>
                    </div>
                </div>
               -->
            </li>
            <li class="notification-dropdown hidden-xs hidden-sm">
                <!--
                <a href="#" class="trigger">
                    <i class="icon-envelope"></i>
                </a>
                <div class="pop-dialog">
                    <div class="pointer right">
                        <div class="arrow"></div>
                        <div class="arrow_border"></div>
                    </div>
                    <div class="body">
                        <a href="#" class="close-icon"><i class="icon-remove-sign"></i></a>
                        <div class="messages">
                            <a href="#" class="item">
                                <img src="img/contact-img.png" class="display" />
                                <div class="name">Alejandra Galván</div>
                                <div class="msg">
                                    There are many variations of available, but the majority have suffered alterations.
                                </div>
                                <span class="time"><i class="icon-time"></i> 13 min.</span>
                            </a>
                            <a href="#" class="item">
                                <img src="img/contact-img2.png" class="display" />
                                <div class="name">Alejandra Galván</div>
                                <div class="msg">
                                    There are many variations of available, have suffered alterations.
                                </div>
                                <span class="time"><i class="icon-time"></i> 26 min.</span>
                            </a>
                            <a href="#" class="item last">
                                <img src="img/contact-img.png" class="display" />
                                <div class="name">Alejandra Galván</div>
                                <div class="msg">
                                    There are many variations of available, but the majority have suffered alterations.
                                </div>
                                <span class="time"><i class="icon-time"></i> 48 min.</span>
                            </a>
                            <div class="footer">
                                <a href="#" class="logout">View all messages</a>
                            </div>
                        </div>
                    </div>
                </div>
                -->
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle hidden-xs hidden-sm" data-toggle="dropdown">
                    {{ !empty(Auth::user()->name) ? Auth::user()->name : 'Guest Login' }}
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                	@if (Auth::user())
                	<li>{{ HTML::link('users/'.Auth::user()->id."/edit", "My Profile") }}</li>
                	@endif
                    <!--
                    <li><a href="personal-info.html">Personal info</a></li>
                    <li><a href="#">Account settings</a></li>
                    <li><a href="#">Billing</a></li>
                    <li><a href="#">Export your data</a></li>
                    <li><a href="#">Send feedback</a></li>
                   	-->
                </ul>
            </li>
            <li class="settings hidden-xs hidden-sm" data-toggle="tooltip" data-placement="bottom" title="Company Settings">
                <a href="{{ URL::to('company') }}" role="button">
                    <i class="fa fa-cog"></i>
                </a>
            </li>
            <li class="settings hidden-xs hidden-sm" data-toggle="tooltip" data-placement="bottom" title="Logout">
                <a href="{{ URL::to('logout') }}" role="button">
                    <i class="fa fa-share"></i>
                </a>
            </li>
        </ul>
        @show
    </header>
	@show

	@section('sidebar')
	<div id="sidebar-nav">
        <ul id="dashboard-menu">
            <!--
            <li class="active">
                <div class="pointer">
                    <div class="arrow"></div>
                    <div class="arrow_border"></div>
                </div>
                <a href="index.html">
                    <i class="icon-home"></i>
                    <span>Home</span>
                </a>
            </li>            
            -->
            @foreach (\Interact\Cms\CMSResource::orderBy('rank')->get() as $resource)
            @if ($resource->parent_id == 0)
	            @if (strpos(Request::url(), $resource->name) !== false)
	            <li class="active">
	            	<div class="pointer">
	                    <div class="arrow"></div>
	                    <div class="arrow_border"></div>
	                </div>
	            @else
	            <li>
	            @endif
	            	<a class="dropdown-toggle" href="#">
	            		<i class="fa fa-{{ $resource->icon }}"></i>
	            		<span>{{ $resource->displayname }}</span>
	            		<i class="fa fa-chevron-down icon-chevron-down"></i>
	            	</a>
	            	<ul class="submenu">
	            		<li>{{ HTML::link($resource->name, $resource->displayname." List") }}</li>
	            		<li>{{ HTML::link($resource->name.'/create', "Create ".str_singular($resource->displayname)) }}</li>
	            	</ul>
	            </li>
            @endif
            @endforeach
            
            @if (\Config::get('cms::cms_data')['stats'] == 'true')
            
            @if (strpos(Request::url(), 'stats') !== false)
            <li class="active">
            	<div class="pointer">
                    <div class="arrow"></div>
                    <div class="arrow_border"></div>
                </div>
            @else
            <li>
            @endif
            
                <a href="{{ URL::to('stats') }}">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>Statistics</span>
                </a>
            </li>
            @endif
            @if (\Config::get('cms::cms_data')['roles'] == 'true')
            
            @if (strpos(Request::url(), 'roles') !== false)
            <li class="active">
            	<div class="pointer">
                    <div class="arrow"></div>
                    <div class="arrow_border"></div>
                </div>
            @else
            <li>
            @endif
            
                <a href="{{ URL::to('roles') }}">
                    <i class="fa fa-lock"></i>
                    <span>Roles</span>
                </a>
            </li>
            @endif
        </ul>
    </div>
	@show
	
	@section('errors')
	
	@show
	
	@yield('content')
	
	<!-- scripts -->
	{{ HTML::script('//code.jquery.com/jquery-latest.js') }}
	{{ HTML::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') }}
	
	<script src="{{ asset('packages/interact/cms/js/bootstrap.min.js') }}"></script>
	<!--<script src="{{ asset('packages/interact/cms/js/jquery-ui-1.10.2.custom.min.js') }}"></script>-->
    <script src="{{ asset('packages/interact/cms/js/jquery.fileupload.js') }}"></script>
	{{ HTML::script('packages/interact/cms/js/select2.min.js') }}
	<script src="{{ asset('packages/interact/cms/js/wysihtml5-0.3.0.js') }}"></script>
   	<script src="{{ asset('packages/interact/cms/js/bootstrap-wysihtml5-0.0.2.js') }}"></script>

    <!-- knob -->
    @section('scripts')
    <script src="{{ asset('packages/interact/cms/js/theme.js') }}"></script>

	@show
    
</body>
</html>