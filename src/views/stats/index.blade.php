@extends('cms::template.main')

@section('styles')
	@parent
	{{ HTML::style('packages/interact/cms/css/compiled/tables.css') }}
@endsection

<!-- will be used to show any messages -->
@if (Session::has('error'))
	<div class="alert-error alert">
		<a class="close" data-dismiss="alert" href="#">&times;</a>
		<strong>{{ Session::get('error') }}</strong>
	</div>
@endif

@section('content')
<div class="content">
	 <!-- upper main stats -->
    <div id="main-stats">
        <div class="row stats-row">
            <div class="col-md-3 col-sm-3 stat">
                <div class="data">
                    <span class="number">{{ $totalviews }}</span>
                    views
                </div>
                <span class="date">Lifetime</span>
            </div>
            <div class="col-md-3 col-sm-3 stat">
                <div class="data">
                    <span class="number">{{ $totalusers }}</span>
                    users
                </div>
                <span class="date">Lifetime</span>
            </div>
            <div class="col-md-3 col-sm-3 stat">
                <div class="data">
                    <span class="number">{{ $last2weeksviews }}</span>
                    views
                </div>
                <span class="date">Last 2 weeks</span>
            </div>
            <div class="col-md-3 col-sm-3 stat last">
                <div class="data">
                    <span class="number">{{ $last2weeksusers }}</span>
                    users
                </div>
                <span class="date">Last 2 weeks</span>
            </div>
        </div>
    </div>
    <!-- end upper main stats -->
	
	<div id="pad-wrapper">
		<div class="row chart">
            <div class="col-md-12">
                <h4 class="clearfix">
                    Last 14 Days
                    <!-- 
                    <div class="btn-group pull-right">
                        <button class="glow left">DAY</button>
                        <button class="glow middle active">MONTH</button>
                        <button class="glow right">YEAR</button>
                    </div>
                   -->
                </h4>
            </div>
            <div class="col-md-12">
                <div id="statschart"></div>
            </div>
        </div>
        
        <div class="table-wrapper media-table section">
				<div class="row head">
			    	<div class="col-md-12">
			        	<h4>Latest Views</h4>
			        </div>
			    </div>
			
				<div class="row filter-block">
		            <div class="pull-right">
		                <!--<div class="ui-select">
		                    <select class="searchselect" data-column="4">
		                    	<option value="">View All</option>
		                    </select>
		                </div>-->
		                <!--<input type="text" class="search" />-->
						{{ HTML::link('stats/download', 'Download CSV', array('class' => 'btn-flat success')) }}
		            </div>
		        </div>
	
				<div class="row table">
					<table class="table table-hover ts-sortable">
						<thead>
							<tr>
								<th>Date</th>
								<!--<th>Time</th>-->
								<th>User</th>
								<th>Email</th>
								<th>Media</th>
							</tr>
						</thead>
						<tbody>
						@foreach($stats as $stat)
							<tr>
								<td class="col-md-3 col-sm-3">{{ date("M d, Y", strtotime($stat->created_at)) }}</td>
								<!--<td>{{ date("g:i a", strtotime($stat->created_at)) }}</td>-->
								<td class="col-md-3 col-sm-3">{{ $stat->user->name }}</td>
								<td class="col-md-3 col-sm-3">{{ $stat->user->email }}</td>
								<td class="col-md-3 col-sm-3">{{ $stat->media->name }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
        
	</div>
</div>
@stop

@section('scripts')
	@parent

	<script src="{{ asset('packages/interact/cms/js/jquery.flot.js') }}"></script>
	<script src="{{ asset('packages/interact/cms/js/jquery.flot.stack.js') }}"></script>
	<script src="{{ asset('packages/interact/cms/js/jquery.flot.resize.js') }}"></script>

	<script type="text/javascript">
	
	$(function(){
		
		var hits = {{ $graphdata }};
		
		var plot = $.plot($("#statschart"),
			[{ data:hits, label: "Hits" }], {
				series: {
					lines: {
						show: true,
						lineWidth: 1,
						fill: true,
						fillColor: {colors: [{opacity: 0.1}, {opacity: 0.13}]}
					},
					points: {
						show: true,
						linewidth: 2,
						radius: 3
					},
					shadowSize: 0,
					stack: true
				},
				grid: {
					hoverable: true,
					clickable: true,
					tickColor: "#f9f9f9",
					borderWidth: 0
				},
				legend: {
					show: false,
					labelBoxBorderColor: "#fff"
				},
				colors: ["#a7b5c5", "#30a0eb"],
				xaxis: {
					ticks: {{ $xaxis }},
                    font: {
                        size: 12,
                        family: "Open Sans, Arial",
                        variant: "small-caps",
                        color: "#697695"
                    }
				},
				yaxis: {
					ticks:3, 
                    tickDecimals: 0,
                    font: {size:12, color: "#9da3a9"}
				}
			});
	})
	</script>

@endsection