@extends('cms::template.main')

@section('styles')
	@parent
	<!-- this page specific styles -->
	{{ HTML::style('packages/interact/cms/css/compiled//user-profile.css') }}
	{{ HTML::style('packages/interact/cms/css/compiled/form-showcase.css') }}
	{{ HTML::style('packages/interact/cms/css/compiled/ui-elements.css') }}
@endsection

@section('content')
<div class="content form-page">
	<div id="pad-wrapper" class="user-profile">
		 <div class="row header">
            <div class="col-md-8">
                <h3 class="name">LDR Spine</h3>
                <span class="area">Company Information</span>
            </div>
        </div>
        
        <div class="row profile">
        	 <div class="col-md-9 bio">
        	 	<div class="profile-box">
        	 		        	 		
 					{{ Form::open(array('url' => 'company/update', 'method' => 'put', 'files' => 'true')) }}
 					<div class="row form-wrapper">
						<div class="col-md-6 column">
							
							<div class="field-box col-md-12">
								{{ Form::label('title', 'About Us Title') }}
								{{ Form::text('title', $company->name, array('class' => 'form-control col-md-9')) }}
							</div>
							
		 					<div class="field-box col-md-12">
							{{ Form::label('file', 'About Us PDF') }}
							{{ Form::file('file') }}
							@if (!empty($company->file))
							  	<i class="fa fa-file"></i>
							 	{{ HTML::link('uploads/file/'.$company->file, 'View File') }}
							@endif
							</div>
							
							<div class="field-box col-md-12">
							{{ Form::label('banner', 'Banner  (1940 x 390)') }}
							{{ Form::file('banner') }}
							@if (!empty($company->banner))
							  	<i class="fa fa-file"></i>
							 	{{ HTML::link('uploads/file/'.$company->banner, 'View File') }}
							@endif
							</div>
						</div>
					</div>
 					
 					<div class="row">
						<!--submit button-->
						{{ Form::submit('Update Company Information', array('class' => 'btn-flat primary pull-right')) }}
					</div>
 					
 					{{ Form::close() }}
        	 		
        	 	</div>
        	 </div>
        </div>		
        
        
	</div>
</div>
@stop