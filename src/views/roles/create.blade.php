@extends('cms::template.main')

@section('styles')
	@parent
	<!-- this page specific styles -->
	{{ HTML::style('packages/interact/cms/css/compiled/form-showcase.css') }}
	{{ HTML::style('packages/interact/cms/css/compiled/ui-elements.css') }}
@endsection

@section('content')
<div class="content form-page">
	<div id="pad-wrapper">
		<div class="row header">
			<h2>New Role</h2>
		</div>
		{{ Form::open(array('url' => 'roles', 'method' => 'post', 'files' => 'true')) }}
		<div class="row section form-wrapper no-gallery">
			<div class="col-md-6 column">
				<div class="field-box col-md-12">
					{{ Form::label('name', 'Role Name') }}
					{{ Form::text('name', '', array('class' => 'form-control col-md-9')) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('expiration', 'Expiration (days)') }}
					{{ Form::text('expiration') }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('active', 'Active') }}
					{{ Form::checkbox('active', true) }}
				</div>
			</div>
		</div>
		<div class="row">
			<!--submit button-->
			{{ Form::submit('Save Role', array('class' => 'btn-flat primary pull-right')) }}
		</div>
		{{ Form::close() }}
	</div>
</div>
@stop