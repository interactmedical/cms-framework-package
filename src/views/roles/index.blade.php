@extends('cms::template.main')

@section('styles')
	@parent
	<!-- this page specific styles -->
	{{ HTML::style('packages/interact/cms/css/compiled/tables.css') }}
@endsection

<!-- will be used to show any messages -->
@if (Session::has('error'))
	<div class="alert-error alert">
		<a class="close" data-dismiss="alert" href="#">&times;</a>
		<strong>{{ Session::get('error') }}</strong>
	</div>
@endif

@section('content')
<div class="content">
	<div class="row">
		<div id="pad-wrapper">
			<div class="table-wrapper roles-table section">
				<div class="row head">
			    	<div class="col-md-12">
			        	<h4>Roles</h4>
			        </div>
			    </div>
				
				<div class="row filter-block">
		            <div class="pull-right">
		                <!--<div class="ui-select">
		                    <select class="searchselect" data-column="4">
		                    	<option value="">View All</option>
		                    </select>
		                </div>-->
		                <!--<input type="text" class="search" />-->
						{{-- HTML::link('roles/create', '+ Add Role', array('class' => 'btn-flat success new-user')) --}}
		            </div>
		        </div>
	
				<div class="row table table-products">
					<table class="table table-hover ts-sortable">
						<thead>
							<tr>
								<th class="col-md-4">Name</th>
								<th class="col-md-4">Expiration</th>
								<th class="col-md-4">Active</th>
							</tr>
						</thead>
						<tbody>
						@foreach($roles as $key => $value)
							<tr>
								<td>{{ HTML::link('roles/'.$value->id.'/edit', $value->name, array('class' => '')) }}</td>
								<td>{{ $value->expiration ?: 0 }} day(s)</td>
								<td>
									@if ($value->active)
									<span class="label label-success">Active</span>
									@else
									<span class="label label-info">Inactive</span>
									@endif
									<ul class="actions">
										<li><a href="{{ URL::to('roles/'.$value->id.'/edit') }}"><i class="table-edit"></i></a></li>
										<li class="last"><a href="{{ URL::to('roles/'.$value->id) }}" class="delete"><i class="table-delete"></i></li>
									</ul>				
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop