@extends('cms::template.main')

@section('styles')
	@parent
	<!-- this page specific styles -->
	{{ HTML::style('packages/interact/cms/css/compiled/signin.css') }}
@endsection

@section('menu-drawer')
	<ul></ul>
@endsection

@section('sidebar')

@endsection

@section('content')

	<div class="row-fluid login-wrapper">
		<div class="box">
			<div class="content-wrap">
			{{ Form::open(array('action' => 'Interact\Cms\LoginController@processLogin', 'method' => 'post')) }}
			
				<!--check for login errors flash -->
				@if (Session::has('login_errors'))
					<div class="alert-error alert">
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<strong>Username or password incorrect</strong>
					</div>
				@endif
				
				@if (Session::has('error'))
					<div class="alert-error alert">
						<a class="close" data-dismiss="alert" href="#">&times;</a>
						<strong>{{ Session::get('error') }}</strong>
					</div>
				@endif
				<!--username field-->
				{{ Form::text('email', '', array('class' => 'span12', 'placeholder' => 'Email')) }}
				
				<!--password field-->
				{{ Form::password('password', array('class' => 'span12', 'placeholder' => 'Password')) }}
				
				<!--submit button-->
				<div class="input">{{ Form::submit('Log In', array('class' => 'btn-glow primary login')) }}</div>
				
			{{ Form::close() }}
				
			<!--<p>New User? {{ HTML::link('register', 'Register Here') }}</p>-->
			</div>
		</div>
	</div>
@endsection