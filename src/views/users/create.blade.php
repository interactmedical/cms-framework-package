@extends('cms::template.main')

@section('styles')
	@parent
	<!-- this page specific styles -->
	{{ HTML::style('packages/interact/cms/css/compiled/form-showcase.css') }}
	{{ HTML::style('packages/interact/cms/css/compiled/ui-elements.css') }}
@endsection

@section('content')
<div class="content form-page">
	<div id="pad-wrapper">
		<div class="row header">
			<h2>New User</h2>
		</div>
		{{ Form::open(array('url' => 'users', 'method' => 'post', 'files' => 'true')) }}
		<div class="row section form-wrapper no-gallery">
			<div class="col-md-6 column">
				<div class="field-box col-md-12">
					{{ Form::label('name', 'User Name') }}
					{{ Form::text('name', '', array('class' => 'form-control col-md-9')) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('email', 'User Email') }}
					{{ Form::text('email', '', array('class' => 'form-control col-md-9')) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('password', 'Password') }}
					{{ Form::password('password', '', array('class' => 'form-control col-md-9')) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('role', 'Role') }}
					{{ Form::select('role[]', Role::select()) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('active', 'Active') }}
					{{ Form::checkbox('active', true) }}
				</div>
			</div>
		</div>
		<div class="row">
			<!--submit button-->
			{{ Form::submit('Save User', array('class' => 'btn-flat primary pull-right')) }}
		</div>
		{{ Form::close() }}
	</div>
</div>
@stop