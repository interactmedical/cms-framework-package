<?php

class Plurals {
	
	public static function setPlurals() {
		$irregular = \Config::get('cms::plurals');
		
		$reflection = new\ReflectionProperty('Illuminate\\Support\\Pluralizer', 'irregular');
		$reflection->setAccessible(true);
		$oldIrregular = $reflection->getValue();
		$reflection->setValue(null, array_merge($oldIrregular, $irregular));
	}
	
}
