<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Interact\Cms\MigrationGenerator;
use Interact\Cms\ViewsGenerator;
use Interact\Cms\ModelGenerator;
use Interact\Cms\ControllerGenerator;
use Illuminate\Support\Pluralizer;
use Illuminate\Filesystem\Filesystem as File;

class create_cms_resource extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'create_cms_resource';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creates new Interact CMS Framework Resource instances';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	 
	 protected $modelgenerator;
	 protected $migrationgenerator;
	 protected $viewsgenerator;
	 protected $controllergenerator;
	 protected $file;
	 
	public function __construct() {
		parent::__construct();
		$this->migrationgenerator = new MigrationGenerator;
		$this->viewsgenerator = new ViewsGenerator;
		$this->modelgenerator = new ModelGenerator;
		$this->controllergenerator = new ControllerGenerator;
		$this->file = new File;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$name = strtolower(Pluralizer::plural($this->argument('name')));
		$parent = strtolower($this->option('parent'));
		
		$this->info('Creating New CMS Object');
		// Create Migration
		$this->info('Creating New Migration File');
		$this->migrationgenerator->generateResourceMigration($name);
		//$this->call('migrate:make', array('argument' => 'create_'.$name.'_table', '--option' => 'table='.$name.', create'));
		// Add Migration to DB
		$this->info('Adding Migration to Database');
		$this->call('migrate', array('--path' => 'app/migrations'));
		// Create Model
		$this->info('Creating Model');
		$this->modelgenerator->generateCmsModel($name);
		// Create Resource Controller
		$this->info('Creating Resource Controller');
		$this->controllergenerator->generateCmsController($name, $parent);
		//$this->call('controller:make', array('name' => ucwords($name)."Controller"));
		
		// Adding Resource Controller call to routes
		$this->info('Adding Resource to Routes');
		$this->file->append(
			app_path().'/routes.php',
			"\n\nRoute::controller('".$name."', '".ucwords(str_singular($name))."Controller');"
		);
		
		// Create Views for Resource Controller
		$this->info('Creating Views');
		$this->viewsgenerator->generateViews($name, $parent);
		
		// Adding Resource to resources table
		$this->info('Adding Resource to Table');
		DB::table('cmsresources')->insert(array('name' => $name, 'displayname' => ucwords($name), 'created_at' => date('Y-m-d')));
				
		$this->info('Resource "'.$name.'" has been added successfully.');
				
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('name', InputArgument::REQUIRED, 'New Object Name')
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('parent', null, InputOption::VALUE_OPTIONAL, 'Parent Resource Name')
		);
	}

}
