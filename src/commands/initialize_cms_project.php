<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Interact\Cms\MigrationGenerator;
use Interact\Cms\ModelGenerator;

class initialize_cms_project extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'initialize_cms_project';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Initialize New CMS';

	protected $generator;
	protected $modelgenerator;


	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->generator = new MigrationGenerator;
		$this->modelgenerator = new ModelGenerator;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Creating New CMS Project');
				
		$this->info('Creating migrations folder');
		$this->generator->makeDir('app/migrations');
		$this->info('Creating Resources Table');
		$this->generator->generateResourcesMigration();
		
		$this->info('Creating Updates Table');
		$this->generator->generateUpdatesMigration();
		
		//TODO: Create Users table and add admin user
		
		$this->call('migrate', array('--path' => 'app/migrations'));
		$this->info('Adding User to Resources Table');
		DB::table('resources')->insert(array('name' => 'users', 'created_at' => date('Y-m-d')));
		
		$this->info('New CMS base tables generated successfully.');
		
		$this->call('asset:publish', array('--bench' => 'interact/cms'));
		//$this->call('config:publish', array('--bench' => 'interact/cms'));
		$this->info('Assets and Configuration have been generated');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
