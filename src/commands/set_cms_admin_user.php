<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class set_cms_admin_user extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'set_cms_admin_user';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sets/Resets admin user for CMS';


	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$admin = \Interact\Cms\User::where('name', '=', 'admin')->first();
		$email = $this->argument('email');
		$password = str_random(12);
		
		if (empty($admin)) {
			$admin = new \Interact\Cms\User;
			$admin->name = 'admin';
		} else {
			$email = $admin->email;
		}
		
		$admin->password = Hash::make($password);
		$admin->email =$email;
		$admin->save();	
		$message = Swift_Message::newInstance('Password Reset')
			->setFrom(array('cms@interactm.com' => 'CMS Password Reset'))
			->setTo(array($email => 'Admin User'))
			->setBody("The CMS system has reset your password to: ".$password);
		
		var_dump($password);
		
		$this->info('New password has been sent');
		return false;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('email', InputArgument::REQUIRED, 'Admin email address (ignored if admin exists)')
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
