<?php

namespace Interact\Cms;

class Stat extends \Eloquent {
	
	public function media() {
		return $this->belongsTo('Media', 'obj_id');
	}
	
	public function user() {
		return $this->belongsTo('User', 'user_id');
	}
	
}
