<?php

namespace Interact\Cms;

class CMSModel extends \Eloquent {
	
	protected $action;
	protected $softDelete = true;
	
	public function save(array $options = array()) {
		$this->action = $this->exists ? 'updated' : 'created';
		parent::save();
		
 		Update::addRecord($this->action, get_class($this), $this->id);
	}
	
	public function delete() {

		Update::addRecord('deleted', get_class($this), $this->id);	
		parent::delete();
	}
	
	public static function select() {
		$selectdata = array();
		$types = parent::all();
		foreach ($types as $type) {
			$selectdata[$type->id] = $type->name;
		}
		return $selectdata;
	}
	
	public function search() {
		
	}
	
	public function upload($type, $file) {
		
		$filename = '';	
		
		if (!empty($file)) {
			
			$name = $file->getClientOriginalName();
			$extension = $file->guessClientExtension();
			
			if (!empty($extension)) {
				$filename = $type.'-'.preg_replace('/[\.\s]/', '', microtime()).'.'.$extension;
				$file->move(base_path()."/public/uploads/".$type.'/', $filename);
				$this->$type = $filename;
			}
	
			$objtype = \Input::get('type');
			if (empty($objtype) && $type == 'file') {
				$this->type = ucwords(str_plural(strstr($file->getClientMimeType(), '/', true)));
			}
		}
			
		return $filename;
	}

	public function uploadMultiple($files) {
		foreach ($files as $key => $file) {
			$this->upload($key, $file);	
		}
	}
	
	public function resource() {
		return CMSResource::where('name', '=', str_plural(strtolower((get_class($this)))))->first();
	}
		
	public function syncRoles($roles) {
		$this->detachRoles();
		$this->attachRoles($roles);
	}
	
	public function attachRoles($roles) {
		foreach($roles as $role) {
			\DB::table('resource_role')->insert(array('role_id' => $role, 'resource_id' => $this->resource()->id, 'obj_id' => $this->id));
		}	
	}
	
	public function detachRoles() {
		\DB::table('resource_role')->where('resource_id', '=', $this->resource()->id)->where('obj_id', '=', $this->id)->delete();
	}
	
	public function roles() {
		$rolesarray = \DB::table('resource_role')->where('resource_id', '=', $this->resource()->id)->where('obj_id', '=', $this->id)->lists('role_id');
		return $rolesarray;
	}
	
	public function authorized() {
		
		//Check for the roles configuration. If there is no roles directive, everything is allowed.
		$config = \Config::get('cms::cms_data');
		if (empty($config['roles'])) {
			return true;
		}		
		
		if (count($this->roles()) == 0) {
			return true;
		}
		
		//Loop through the roles table and return results. If any one result returns true, the resource is allowed.		
		foreach (\Auth::user()->roles as $role) {
			if (in_array($role->id, $this->roles())) {
				return true;
			}
		}
		 
		return false;
	}
}
