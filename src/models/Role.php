<?php 

class Role extends \Eloquent {
	
	public function users() {
		return $this->belongsToMany('Role');
	}
	
	public static function select() {
		$selectdata = array();
		$types = parent::all();
		foreach ($types as $type) {
			$selectdata[$type->id] = $type->name;
		}
		return $selectdata;
	}
	
}
