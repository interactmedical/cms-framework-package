<?php 

namespace Interact\Cms;
use \Lang;

class Update extends \Eloquent {
	
	public $data = array();
	protected $globals = array();
	protected $timestamp;
	protected $user;
	protected $protectedkeys = array('class', 'arguments');
	
	public function resources() {
		return $this->belongs_to('Resource');
	}
	
	
	/**
	  * Add a new record to the updates table
	  *
	  * @param action	The action (create, update, delete)
	  * @param class 	The object class we are updating
	  * @param key		The id of the object
	  */
	
	public static function addRecord($action, $class, $key) {
		$update = new Update;
		$update->resource_id = CMSResource::where('name', '=', str_plural($class))->first()->id;
		$update->object_id = $key;
		$update->user_id = \Auth::user()->id;
		$update->type = $action;
		$update->save();
	}
	
	public function getJsonStructure($structure, $timestamp = '', $user) {
		
		$this->user = $user;
		$this->timestamp = intval($timestamp);
		
		$this->data['InteractLiveContentTimestamp'] = strtotime(date('Y-m-d\TH:i:s\Z'));
		
		if ($this->getUpdateCount() <= 0 && !empty($timestamp)) {
			return json_encode($this->data);
		}		
				
		foreach ($structure as $resourcekey => $resourcevalue) {
			$this->data[$resourcekey] = array();	
			$structureitem = $this->recurseBase($resourcekey, $resourcevalue);
			foreach ($structureitem as $item) {
				$this->data[$resourcekey] = $item;	
			}
		}
		return json_encode($this->data);		
	}
	
	private function recurseBase($resourcekey, $resourcevalue, $parent = '', $obj = '') {
					
		$dataarray = array();
		$dataarray[$resourcekey] = array();
		$arguments = !empty($resourcevalue['arguments']) ? $resourcevalue['arguments'] : '';
				
		if (!empty($resourcevalue['class'])){
			$root = $resourcevalue['class'];
			$items = $this->getDataSet($root, $parent, $arguments, $obj); //Gets only updates
			$this->currenttree = $items;
			if (count($items) == 0 && !empty($resourcevalue['arguments']['nonresource'])) {
				$itemarray = $this->processChildRecursions($resourcevalue, $parent);
				$dataarray[$resourcekey] = $itemarray;
			} else {
				foreach ($items as $item) {
					if (empty($resourcevalue['arguments']['nonresource'])) {
						if (!method_exists($item, 'authorized') || $item->authorized()==true) {
							$itemarray = $this->processChildRecursions($resourcevalue, $item->id, $item);					
							array_push($dataarray[$resourcekey], $itemarray);
						}
					} else {
						if ($root == "Role") {
							foreach (\Auth::user()->roles as $role) {
								if ($role->id == $item->id) {
									if (count(\Auth::user()->roles) > 1) {
										$itemarray = $this->processChildRecursions($resourcevalue, $item->id, $item);
										array_push($dataarray[$resourcekey], $itemarray);
									} else {
										$itemarray = $this->processChildRecursions($resourcevalue, $item->id, $item);
										$dataarray[$resourcekey] = $itemarray;
									}
								} 
							}
						} else {
							$itemarray = $this->processChildRecursions($resourcevalue, $item->id, $item);
							$dataarray[$resourcekey] = $itemarray;
						}
					}
				}
			}
		} else {
			$itemarray = $this->processChildRecursions($resourcevalue, $parent);
			$dataarray[$resourcekey] = $itemarray;
		}
		
		return $dataarray;
	}
	
	protected function processChildRecursions($resourcevalue, $parent, $obj = '') {
				
		$itemarray = array();
		
		foreach ($resourcevalue as $key => $value) {
			$data = array();
			if (!in_array($key, $this->protectedkeys)) {
				if (is_array($value)) {
					
					if (!empty($resourcevalue['arguments']['global'])) {
						foreach ($resourcevalue['arguments']['global'] as $globalkey => $globalvalue) {
							$globalFunction = $obj;
							foreach($globalvalue as $scope) {
								$globalFunction = $globalFunction->$scope;
							}
							$this->globals[$globalkey] = $globalFunction;
						}
					}
					
					$itemarray[$key] = array();
					$data = $this->recurseBase($key, $value, $parent, $obj);
				} else {
					$itemarray[$key] = array();
					$data = $this->getFields($obj, $key, $value);
				}
			}
				
			foreach ($data as $data) {
				$itemarray[$key] = $data;	
			}			
		}
		return $itemarray;
	}
	
	protected function getDataSet($root, $parent = '', $arguments = array(), $obj = '') {
						
		$results = $root::orderBy('rank')->where('active', '=', true);
												
		if (!empty($parent)) {
			if (!empty($arguments['relationship'])) {
				$relationship = $arguments['relationship']['relationship'];
				$relation_field = $arguments['relationship']['relation_field'];
				$relation_value = $arguments['relationship']['relation_parent_value'];
				
				$results = $results->whereHas($relationship, function($query) use ($relation_field, $relation_value, $obj) {
						$query->where($relation_field, '=', $obj->$relation_value);	
				});
									
			} else {
				$results = $results->where('parent_id', '=', $parent);	
			}
			
		}
								
		if (!empty($arguments['query'])) {
			foreach ($arguments['query'] as $field => $value) {
				if (is_callable($value)) {
					$value = $value($this->globals);
				} 
								
				$results = $results->where($field, '=', $value);
				
				
			}
		}
	
		return $results->get();		
	}
	
	protected function getFields($obj, $pairkey, $pairvalue) {
																								
		$objdata = array();
				
		switch ($pairvalue) {
			case 'file' :
				$objdata[$pairkey] = array();		
				$objdata[$pairkey]['InteractLiveContentFileName'] = !empty($obj->filename) ? $obj->filename : $obj->name;
				$objdata[$pairkey]['InteractLiveContentFileURL'] = asset("uploads/file/".$obj->file);
				$objdata[$pairkey]['InteractLiveContentFileType'] = !empty($obj->type) ? $obj->type : str_plural(get_class($obj));
				$objdata[$pairkey]['InteractLiveContentFileFlag'] = ($obj->autodownload == 0) ? true : false;
				break;
			case 'thumb' :
				$objdata[$pairkey] = array();
				$objdata[$pairkey]['InteractLiveContentFileFlag'] = true;
				$objdata[$pairkey]['InteractLiveContentFileURL'] = asset("uploads/thumb/".$obj->thumb);
				break;
			default :
				$objdata[$pairkey] = array();
				if (!empty($obj)) {
					$objdata[$pairkey] = is_null(json_decode($obj->$pairvalue)) ? $obj->$pairvalue : json_decode($obj->$pairvalue);	
				} else {
					$objdata[$pairkey] = '';
				}
				break;
		}
			
			
		return $objdata;
	}
	
	protected function getUpdateCount() {
		$updates = Update::where('created_at', '>=', date("Y-m-d H:i:s", $this->timestamp))->count();
		return $updates;
	}
	
}
