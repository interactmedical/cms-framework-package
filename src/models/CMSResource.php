<?php

namespace Interact\Cms;

class CMSResource extends \Eloquent {

	protected $table = "cmsresources";

	public function updates() {
		return $this->hasMany('Update');
	}

	public function resource() {
		return $this->belongsTo('\Interact\Cms\CMSResource', 'parent_id');
	}
	
	public function resources() {
		return $this->hasMany('\Interact\Cms\CMSResource', 'parent_id');
	}

}