<?php

namespace Interact\Cms;

class ViewsGenerator extends BaseGenerator {
	
	public function generateViews($name, $parent) {
		$this->name = $name;
		$this->createViewDirectory();
		if (!empty($parent)) {
			$this->createChildViews();
		} else {
			$this->createViews();	
		}
	}
	
	public function createViewDirectory() {
		$this->path = $this->makeDir('app/views/'.$this->name);
	}
	
	public function createViews() {
		$views = array('create', 'index', 'edit');
		foreach ($views as $file) {
			$template = $this->getTemplate('views/parent', $file);
			$this->makeFile($file.".blade", $template);
		}	
	}
	
	public function createChildViews() {
		$views = array('create', 'edit');
		foreach ($views as $file) {
			$template = $this->getTemplate('views/child', $file);
			$this->makeFile($file.".blade", $template);
		}
	}
	
}
