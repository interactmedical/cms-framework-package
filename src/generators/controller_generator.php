<?php

namespace Interact\Cms;

class ControllerGenerator extends BaseGenerator {

	public function generateCmsController($name, $parent = '') {
		$this->name = $name;
		$this->path = 'app/controllers';
		$template = !empty($parent) ? $this->getTemplate('controllers/child', 'resource_controller') : $this->getTemplate('controllers/parent', 'resource_controller');
		$this->makeFile(ucfirst(str_singular($this->name))."Controller", $template);
	}

}
