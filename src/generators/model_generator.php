<?php

namespace Interact\Cms;

class ModelGenerator extends BaseGenerator {

	public function generateCmsModel($name) {
		$this->name = $name;
		$this->path = 'app/models';
		$template = $this->getTemplate('models', 'cms_object');
		$this->makeFile(ucfirst(str_singular($this->name)), $template);
	}
	
	public function generateModel($name) {
		$this->name = $name;
		$this->path = 'app/models';
		$template = $this->getTemplate('models', 'eloquent');
		$this->makeFile(ucfirst(str_singular($this->name)), $template);
	}

}
