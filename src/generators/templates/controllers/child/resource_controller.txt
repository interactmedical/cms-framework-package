<?php

class {{ucsingle}}Controller extends \BaseController {
	 
	public function __construct() {
		$this->beforeFilter('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		${{name}} = {{ucsingle}}::orderBy('rank')->get();
		return View::make('{{name}}.index')->with('{{name}}', ${{name}});
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		return View::make('{{name}}.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postStore()
	{
		$input = Input::all();
		$files = Input::file();
		${{namesingle}} = new {{ucsingle}};
		
		${{namesingle}}->name = $input['name'];
		${{namesingle}}->description = !empty($input['description']) ? $input['description'] : '';
		${{namesingle}}->type = !empty($input['type']) ? $input['type'] : '';
		${{namesingle}}->parent_id = !empty($input['parent']) ? $input['parent'] : '';
		${{namesingle}}->active = (!empty($input['active']));
		${{namesingle}}->autodownload = (!empty($input['autodownload']));
		
		${{namesingle}}->uploadMultiple($files);
		
		${{namesingle}}->save();
		
		return Redirect::to('{{name}}');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		${{namesingle}} = {{ucsingle}}::find($id);
		return View::make('{{name}}.edit')->with('{{namesingle}}', ${{namesingle}});
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function putUpdate($id)
	{
		$input = Input::all();
		$files = Input::file();
		${{namesingle}} = {{ucsingle}}::find($id);
		
		${{namesingle}}->name = $input['name'];
		${{namesingle}}->description = !empty($input['description']) ? $input['description'] : '';
		${{namesingle}}->type = !empty($input['type']) ? $input['type'] : '';
		${{namesingle}}->parent_id = !empty($input['parent']) ? $input['parent'] : '';
		${{namesingle}}->active = (!empty($input['active']));
		${{namesingle}}->autodownload = (!empty($input['autodownload']));
		
		${{namesingle}}->uploadMultiple($files);
		
		${{namesingle}}->save();
		
		return Redirect::to('{{name}}');
	}

	/**
	 * Get the specified resource child count.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($id)
	{
		//echo count({{ucsingle}}::find($id)->CHILDNAME);
		echo "0";
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteDelete($id)
	{
		${{namesingle}} = {{ucsingle}}::find($id);
		Session::flash('message', ${{namesingle}}->name." has been deleted.");
		${{namesingle}}->delete();
	}

	public function postSort() {
		$input = Input::all();
		foreach ($input['{{namesingle}}'] as $order => $id) {
			${{namesingle}} = {{ucsingle}}::find($id);
			${{namesingle}}->rank = $order;
			${{namesingle}}->save();
		}
	}

}