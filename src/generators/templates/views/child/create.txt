@extends('cms::template.main')

@section('styles')
	@parent
	<!-- this page specific styles -->
	{{ HTML::style('packages/interact/cms/css/compiled/form-showcase.css') }}
	{{ HTML::style('packages/interact/cms/css/compiled/ui-elements.css') }}
@endsection

@section('content')
<div class="content form-page">
	<div id="pad-wrapper">
		<div class="row header">
			<h2>New {{ucsingle}}</h2>
		</div>
		{{ Form::open(array('url' => '{{name}}/store', 'method' => 'post', 'files' => 'true')) }}
		<div class="row section form-wrapper no-gallery">
			<div class="col-md-6 column">
				<div class="field-box col-md-12">
					{{ Form::label('name', '{{ucsingle}} Name') }}
					{{ Form::text('name', '', array('class' => 'form-control col-md-9')) }}
				</div>
				<div class="field-box col-md-12 textarea">
					{{ Form::label('description', 'Description') }}
					{{ Form::textarea('description', '', array('class' => 'form-control col-md-9')) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('type', 'Type') }}
					{{ Form::select('type', array(), '', array('class' => 'filter init-select')) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('active', 'Active') }}
					{{ Form::checkbox('active', true, true) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('autodownload', 'On Demand') }}
					{{ Form::checkbox('autodownload', true) }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('file', 'Asset File') }}
					{{ Form::file('file') }}
				</div>
				<div class="field-box col-md-12">
					{{ Form::label('thumb', 'Thumb') }}
					{{ Form::file('thumb') }}
				</div>
			</div>
		</div>
		<div class="row">
			<!--submit button-->
			{{ Form::submit('Save {{ucsingle}}', array('class' => 'btn-glow primary pull-right')) }}
		</div>
		{{ Form::close() }}
	</div>
</div>
@stop