<?php

namespace Interact\Cms;

class MigrationGenerator extends BaseGenerator {

	public function __construct() {
		parent::__construct();
		$this->path = 'app/migrations';
	}

	public function generateResourcesMigration() {
		$template = $this->getTemplate('migrations', 'create_resources_table');
		$filename = $this->createMigrationFilename('create_resources_table');
		$this->makeFile($filename, $template);
	}
	
	public function generateUpdatesMigration() {
		$template = $this->getTemplate('migrations', 'create_updates_table');
		$filename = $this->createMigrationFilename('create_updates_table');
		$this->makeFile($filename, $template);
	}

	public function generateResourceMigration($name) {
		$this->name = $name;
		$template = $this->getTemplate('migrations', 'default_cms_object_migration');
		$filename = $this->createMigrationFilename(strtolower('create_'.$this->name."_table"));
		$this->makeFile($filename, $template);
	}
	
	public function createMigrationFilename($filename) {
		$filename = date('Y_m_d_His').'_'.$filename;
		return $filename;
	}

}
