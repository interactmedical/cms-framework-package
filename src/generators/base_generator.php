<?php

namespace Interact\Cms;

use Illuminate\Filesystem\Filesystem as File;

class BaseGenerator {
	
	public $path;
	protected $file;
	public $name;
	
	public function __construct() {
		$this->file = new File;
	}
	
	public function makeDir($path) {
		mkdir($path, 0775);
		return $path;
	}
	
	public function makeFile($filename, $template) {
		$filename = $this->path."/".$filename.".php";
		
		if (!$this->file->exists($filename)) {
			$this->file->put($filename, $template) !== false;
		}
	}
	
	public function getTemplate($type, $template) {
		$stub = $this->file->get(__DIR__."/templates/".$type."/".$template.'.txt');
		$stub = str_replace('{{name}}', $this->name, $stub);
		$stub = str_replace('{{namesingle}}', str_singular($this->name), $stub);
		$stub = str_replace('{{ucname}}', ucwords($this->name), $stub);
		$stub = str_replace('{{ucsingle}}', ucwords(str_singular($this->name)), $stub);
		return $stub;
	}
	
}
