<?php

//Live Routes
Route::get('/', array('before' => 'auth', function()
{
	return \View::make('cms::index');
}));

Route::get('login', 'Interact\Cms\LoginController@showLogin');
Route::post('login', 'Interact\Cms\LoginController@processLogin');
Route::get('logout', 'Interact\Cms\LoginController@logout');

Route::any('update', 'Interact\Cms\UpdateController@postUpdate');
Route::controller('company', 'Interact\Cms\CompanyController');

Route::any('filters', 'Interact\Cms\FilterController@getFilterRules');
//Controller Route
Route::resource('users', 'Interact\Cms\UserController');
Route::resource('roles', 'Interact\Cms\RoleController');

//Route Prefixes

//Error Routes
