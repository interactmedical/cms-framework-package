<?php
namespace Interact\Cms;

class UpdateController extends \BaseController {

	public function postUpdate(){
		
		$input = \Input::all();
		$time = (!empty($input['InteractLiveContentTimestamp'])) ? $input['InteractLiveContentTimestamp'] : '';
		
		$config = \Config::get('cms::cms_data');
		$token = $config['token'];
		
		$resources = \Config::get('cms::cms_structure');
		
		if (empty($input['InteractLiveContentToken']) || $input['InteractLiveContentToken'] != $token) {
			$error = "The authentication token is invalid.";
			return \View::make('cms::data/error')->with('error', $error);
		} 
		
		if (!\Auth::attempt(array('email' => $input['InteractLiveContentUsername'], 'password' => $input['InteractLiveContentPassword']))) {
			$error = "The username or password are invalid.";
			return \View::make('cms::data/error')->with('error', $error);	
		} 
		
		/* Check for invalid timestamp
		if (($input['InteractLiveContentTimestamp'])) {
			$error = "The update timestamp is invalid.";
			return \View::make('cms::data/error')->with('error', $error);
		}
		*/
		
	
		$user = \Auth::user();	
		return \View::make('cms::data/update')->with('user', $user)->with('resources', $resources)->with('timestamp', $time);
		
	}		
		
}
	