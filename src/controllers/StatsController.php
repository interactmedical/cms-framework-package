<?php
namespace Interact\Cms;

class StatsController extends \BaseController {

	public function upload(){
		
		$input = \Input::all();
		
		$config = \Config::get('cms::cms_data');
		$token = $config['token'];
				
		if (empty($input['InteractLiveContentToken']) || $input['InteractLiveContentToken'] != $token) {
			\App::abort(403, "You are not authorized to view this page");
		} 
		
		if (!\Auth::attempt(array('email' => $input['InteractLiveContentUsername'], 'password' => $input['InteractLiveContentPassword']))) {
			\App::abort(403, "You are not authorized to view this page");
		} 
				
		$obj = \Media::where('file', '=', $input['InteractLiveContentFileName'])->first();
		
		$stat = new Stat;
		$stat->obj_id = $obj->id;
		$stat->user_id = \Auth::user()->id;
		
		$stat->save();
				
	}
	
	public function getDownload() {
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=cayenne-stats-".date("Y-m-d_H-i").".csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$headers = array("Date", "Name", "Email", "Filename");
		
		$stats = \DB::table('stats')
			->select(\DB::raw('stats.created_at, users.name as user, users.email as email, media.name as file'))
			->join('users', 'stats.user_id', '=', 'users.id')
			->join('media', 'stats.obj_id', '=', 'media.id')
			->get();
		
		$out = fopen('php://output', 'w');
		
		fputcsv($out, $headers);
		foreach ($stats as $statkey => $statvalue) {
			$itemarray = array();
			foreach ($statvalue as $stat) {
				array_push($itemarray, $stat);
			}
			fputcsv($out, $itemarray);
		}
		
		fclose($out);
	}
	
	public function getIndex() {
		
		$allstats = Stat::orderBy('created_at', 'desc')->get();
		
		$stats = \DB::table('stats')
			->select(\DB::raw('DAY(created_at) day, COUNT(*) day_count'))
			->groupBy(\DB::raw('day'))
			->where("created_at", ">", date("Y-m-d H:i:s", strtotime("-2 weeks")))
			->get();
			
		$xaxisarray = array();	
		$grapharray = array();			
		for ($i = 0; $i <= 13; $i++) {
			$date = strtotime(-13+$i." days");
			array_push($xaxisarray, array((int)$i+1, date("M j", $date)));
			foreach ($stats as $stat) {
				if ($stat->day == date("j", $date)) {
					array_push($grapharray, array((int)$i+1, (int)$stat->day_count));
				}
			}
		}
		
		$totalusers = count(Stat::groupBy('user_id')->get());
		$last2weeksusers = count(Stat::groupBy('user_id')->where("created_at", ">", date("Y-m-d H:i:s", strtotime("-2 weeks")))->get());
		$totalviews = Stat::count();
		$last2weeksviews = Stat::where("created_at", ">", date("Y-m-d H:i:s", strtotime("-2 weeks")))->count();	
			
				
		return \View::make('cms::stats.index')
			->with('graphdata', json_encode($grapharray))
			->with('xaxis', json_encode($xaxisarray))
			->with('stats', $allstats)
			->with('totalusers', $totalusers)
			->with('last2weeksusers', $last2weeksusers)
			->with('totalviews', $totalviews)
			->with('last2weeksviews', $last2weeksviews);
	}		
		
}
	