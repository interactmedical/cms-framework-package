<?php
namespace Interact\Cms;

class LoginController extends \BaseController {

	public function showLogin() {
		return \View::make('cms::auth.login');
	}
	
	public function processLogin() {
		$credentials = array('email' => \Input::get('email'), 'password' => \Input::get('password'));
			
		if (\Auth::attempt($credentials)) {
			return \Redirect::to('/');
		} else {
			return \Redirect::to('login')->with('login_errors', true);
		}
	}
	
	public function logout() {
		\Auth::logout();
		return \Redirect::to('login');
	}
	
}