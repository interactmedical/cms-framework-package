<?php

namespace Interact\Cms;

class UserController extends \BaseController {

	/**
	 * Master layout
	 */

	 public function __construct() {
		$this->beforeFilter('auth');
	}

	 

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = \User::all();
		return \View::make('cms::users.index')->with('users', $users);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \View::make('cms::users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = \Input::all();
		$user = new \User;
		
		$user->name = $input['name'];
		$user->email = $input['email'];
		$user->password = \Hash::make($input['password']);
		$user->active = (!empty($input['active']));
		
		$user->save();
		
		if (!empty($input['role'])) {
			$user->roles()->sync($input['role']);
		}
		
		return \Redirect::to('users');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return \View::make('cms::users.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = \User::find($id);
		return \View::make('cms::users.edit')->with('user', $user);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = \Input::all();
		$user = \User::find($id);
		
		$user->name = $input['name'];
		$user->email = $input['email'];
 		if (!empty($input['password'])) {
			$user->password = \Hash::make($input['password']);	
		}
		if (!empty($input['role'])) {
			$user->roles()->sync($input['role']);
		}
		$user->active = (!empty($input['active']));
		
		
		$user->save();
		
		return \Redirect::to('users');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = \User::find($id);
		\Session::flash('message', $user->name." has been deleted.");
		$user->delete();
	}

}