<?php
namespace Interact\Cms;

class CompanyController extends \BaseController {

	public function getIndex() {
		$company = \Company::first();
		return \View::make('cms::company.edit')->with('company', $company);
	}
	
	public function putUpdate() {
		$input = \Input::all();
		$files = \Input::file();
		$company = \Company::first();
		
		$company->name = $input['title'];
		
		$company->uploadMultiple($files);
		
		$company->save();
				
		return \Redirect::to('company');	
	}
	
}