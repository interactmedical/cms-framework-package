<?php 
namespace Interact\Cms;

class FilterController extends \BaseController {
	
	public $rules = array();
	
	public function getFilterRules() {
		$input = \Input::get('filter');
		$config = \Config::get('cms::filter_rules');
								
		foreach ($config as $rulename => $rulevalues) {
			$this->rules[$rulename] = in_array($input, $rulevalues);
		}		
		echo json_encode($this->rules);		
	}
	
}
	