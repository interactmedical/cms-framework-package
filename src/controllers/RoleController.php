<?php

namespace Interact\Cms;

class RoleController extends \BaseController {

	public function __construct() {
		$this->beforeFilter('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = \Role::all();
		return \View::make('cms::roles.index')->with('roles', $roles);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return \View::make('cms::roles.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = \Input::all();
		$role = new \Role;
		
		$role->name = $input['name'];
		$role->expiration = $input['expiration'];
		$role->active = (!empty($input['active']));
		
		$role->save();
		
		return \Redirect::to('roles');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return \View::make('cms::roles.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$role = \Role::find($id);
		return \View::make('cms::roles.edit')->with('role', $role);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = \Input::all();
		$role = \Role::find($id);
		
		$role->name = $input['name'];
		$role->expiration = $input['expiration'];
		$role->active = (!empty($input['active']));
		
		$role->save();
		
		return \Redirect::to('roles');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$role = \Role::find($id);
		\Session::flash('message', $role->name." has been deleted.");
		$role->delete();
	}

}